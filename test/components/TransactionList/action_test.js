import expect from 'expect.js';
import { requestTransactions } from '../../../src/actions';
import { RECEIVE_TRANSACTIONS } from '../../../src/constants';
import sinon from 'sinon';

it('Should return action with no transactions', () => {
  const dispatch = sinon.spy();
  requestTransactions(dispatch);
  expect(dispatch.calledWith({
    type: RECEIVE_TRANSACTIONS,
    transactions: []
  })).to.be.true;
});



