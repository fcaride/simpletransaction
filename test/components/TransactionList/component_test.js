import React from 'react';
import { setupDOM } from "../../setupDOM";
import TransactionList from '../../../src/components/TransactionList';
import Transaction from '../../../src/components/Transaction';
import expect from 'expect.js';
import { shallow } from "enzyme";

describe('TransactionList', () => {
  const props = {
    transactions: [
      {
        name: "Fer",
        date: "2016-06-02",
        amount: "10"
      },
      {
        name: "Javier",
        date: "2016-06-03",
        amount: "10"
      },
      {
        name: "Mario",
        date: "2016-06-06",
        amount: "10"
      }
    ]
  }
  beforeEach(() => {
    setupDOM();
  })
  it('should render itself', () => {
    const component = shallow(<TransactionList{...props}/>);
    expect(component.find(Transaction)).to.have.length(3);
  })
})
