import expect from 'expect.js';
import { mainReducer } from '../../../src/reducer';
import { RECEIVE_TRANSACTIONS } from '../../../src/constants';

it('Should add the received transaction to the state', () => {
  const transaction = {
    name: "Pablo",
    date: "2016-06-06",
    amount: "10"
  };
  const receiveTransaction = {
    transactions: [transaction],
    filteredTransactions: [transaction],
    type: RECEIVE_TRANSACTIONS
  };
  const expectedState = {
    transactions: [transaction],
    filteredTransactions: [transaction],
    filterFrom: "",
    filterTo: ""
  };
  expect(mainReducer(undefined, receiveTransaction)).to.eql(expectedState);
});

