import React from 'react';
import { setupDOM } from "../../setupDOM";
import SearchBar from '../../../src/components/SearchBar';
import expect from 'expect.js';
import { mount } from "enzyme";


describe('components', () => {
  beforeEach(() => {
    setupDOM();
  })
  describe('Transaction', () => {
    it('should render itself', () => {
      const component = mount(<SearchBar/>);
      expect(component.find('input').length).to.eql(2);
      expect(component.find('span').length).to.eql(2);
    })
  })
});

