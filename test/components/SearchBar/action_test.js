import expect from 'expect.js';
import { changeFilterFrom, changeFilterTo, filterChanged } from '../../../src/actions';
import { CHANGE_FROM_FILTER, CHANGE_TO_FILTER, FILTER_CHANGED } from '../../../src/constants';
import sinon from 'sinon';

it('Should return changeFilterFrom action', () => {
  const expectedAction = {
    type: CHANGE_FROM_FILTER,
    inputValue: "2016-06-06"
  };
  expect(changeFilterFrom("2016-06-06")).to.eql(expectedAction);
});

it('Should return changeFilterT action', () => {
  const expectedAction = {
    type: CHANGE_TO_FILTER,
    inputValue: "2016-06-06"
  };
  expect(changeFilterTo("2016-06-06")).to.eql(expectedAction);
});


it('Should return filterChanged action with only one transaction', () => {
  const transactions = [{
    name: "Pablo",
    date: "2016-06-06",
    amount: "10"
  },
    {
      name: "Raul",
      date: "2016-06-09",
      amount: "10"
    }];

  const expectedTransactions = [{
    name: "Pablo",
    date: "2016-06-06",
    amount: "10"
  }];
  const dispatch = sinon.spy();
  const getState = () => ({
    main: {
      filterFrom: "2016-06-05",
      filterTo: "2016-06-07",
      transactions
    }
  });
  filterChanged(dispatch, getState);
  expect(dispatch.calledWith({
    type: FILTER_CHANGED,
    filteredTransactions: expectedTransactions
  })).to.be.true;
});


