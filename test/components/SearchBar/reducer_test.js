import expect from 'expect.js';
import { mainReducer } from '../../../src/reducer';
import { CHANGE_FROM_FILTER, CHANGE_TO_FILTER, FILTER_CHANGED } from '../../../src/constants';

it('Should change filterFrom of the state', () => {
  const receiveTransaction = {
    inputValue: "2016-06-06",
    type: CHANGE_FROM_FILTER
  };
  const expectedState = {
    transactions: [],
    filteredTransactions: [],
    filterFrom: "2016-06-06",
    filterTo: ""
  };
  expect(mainReducer(undefined, receiveTransaction)).to.eql(expectedState);
});

it('Should change filterTo of the state', () => {
  const receiveTransaction = {
    inputValue: "2016-06-06",
    type: CHANGE_TO_FILTER
  };
  const expectedState = {
    transactions: [],
    filteredTransactions: [],
    filterTo: "2016-06-06",
    filterFrom: ""
  };
  expect(mainReducer(undefined, receiveTransaction)).to.eql(expectedState);
});

it('Should refresh the filteredTransactions to the state', () => {
  const transaction = {
    name: "Pablo",
    date: "2016-06-06",
    amount: "10"
  };
  const receiveTransaction = {
    filteredTransactions: [transaction],
    type: FILTER_CHANGED
  };
  const expectedState = {
    transactions: [],
    filteredTransactions: [transaction],
    filterFrom: "",
    filterTo: ""
  };
  expect(mainReducer(undefined, receiveTransaction)).to.eql(expectedState);
});

