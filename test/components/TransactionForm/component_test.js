import React from 'react';
import { validate } from '../../../src/components/TransactionForm';
import expect from 'expect.js';

describe('TransactionForm', () => {
  it('should return no errors', () => {
    expect(validate({
      name: "Fer",
      date: "2017-02-03",
      amount: 10
    })).to.be.true;
  })
  it('should return Please provide a name', () => {
    expect(validate({
      name: "",
      date: "2017-02-03",
      amount: 10
    })).to.eql({
      name: "Please provide a name"
    });
  })
  it('should return Please provide a real name', () => {
    expect(validate({
      name: "Fer123",
      date: "2017-02-03",
      amount: 10
    })).to.eql({
      name: "Please provide a real name"
    });
  })
  it('should return Please provide a date', () => {
    expect(validate({
      name: "Fer",
      date: "",
      amount: 10
    })).to.eql({
      date: "Please provide a date"
    });
  })
  it('should return Please provide a real date', () => {
    expect(validate({
      name: "Fer",
      date: "201ads7-02-03",
      amount: 10
    })).to.eql({
      date: "Please provide a real date"
    });
  })
  it('should return Please provide a date in the future', () => {
    expect(validate({
      name: "Fer",
      date: "2015-02-03",
      amount: 10
    })).to.eql({
      date: "Please provide a date in the future"
    });
  })
  it('should return Please provide an amount', () => {
    expect(validate({
      name: "Fer",
      date: "2017-02-03",
      amount: ""
    })).to.eql({
      amount: "Please provide an amount"
    });
  })
  it('should return Please provide a real amount', () => {
    expect(validate({
      name: "Fer",
      date: "2017-02-03",
      amount: "asd"
    })).to.eql({
      amount: "Please provide a real amount"
    });
  })
})
