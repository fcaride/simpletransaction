import expect from 'expect.js';
import { mainReducer } from '../../../src/reducer';
import { ADD_TRANSACTION } from '../../../src/constants';

it('Should add the transaction to the state', () => {
  const transaction = {
    name: "Pablo",
    date: "2016-06-06",
    amount: "10"
  };
  const addTransaction = {
    transaction,
    type: ADD_TRANSACTION
  };
  const expectedState = {
    transactions: [transaction],
    filteredTransactions: [transaction],
    filterFrom: "",
    filterTo: ""
  };
  expect(mainReducer(undefined, addTransaction)).to.eql(expectedState);
});



