import expect from 'expect.js';
import { addTransaction } from '../../../src/actions';
import { ADD_TRANSACTION } from '../../../src/constants';
import sinon from 'sinon';

it('should create the add action', () => {
  const expectedTransaction = {
    name: "Pablo",
    date: "2016-06-06",
    amount: "10"
  };
  const dispatch = sinon.spy();
  const getState = () => ({
    form: {
      transaction: {
        name: {
          value: "Jorge"
        },
        date: {
          value: "2016-06-06"
        },
        amount: {
          value: "10"
        }
      }
    }
  });
  addTransaction(dispatch, getState);
  expect(dispatch.calledWith({
    type: ADD_TRANSACTION,
    transaction: expectedTransaction
  })).to.be.true;
});
