import React from 'react';
import { setupDOM } from "../../setupDOM";
import Transaction from '../../../src/components/Transaction';
import expect from 'expect.js';
import { mount } from "enzyme";


describe('components', () => {
  const props = {
    name: "Fer",
    date: "2016-06-06",
    amount: "10"
  }
  beforeEach(() => {
    setupDOM();
  })
  describe('Transaction', () => {
    it('should render itself', () => {
      const component = mount(<Transaction{...props}/>);
      expect(component.props().name).to.eql("Fer");
      expect(component.props().date).to.eql("2016-06-06");
      expect(component.props().amount).to.eql("10");

    })
  })
});

