import jsdom from 'jsdom';

export function setupDOM() {
  if (typeof document === 'undefined') {
    global.document = jsdom.jsdom('<html><body></body></html>');
    global.window = document.defaultView;
    global.navigator = window.navigator;
  }
}