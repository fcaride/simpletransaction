import { createStore, applyMiddleware, compose } from 'redux';
import thunkMiddleware from 'redux-thunk';
import reducer from './reducer';
import DevTools from './components/DevTools';

const middleWare = [thunkMiddleware];
const store = createStore(reducer, {},
  compose(
    applyMiddleware(...middleWare),
    DevTools.instrument()
  )
);

export default function configureStore() {
  return store;
}
