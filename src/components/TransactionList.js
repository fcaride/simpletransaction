import React from 'react';
import Transaction from './Transaction';
import { Link } from 'react-router';
import SearchBarContainer from './../containers/SearchBarContainer'

const TransactionList = ({transactions}) => {

  const transactionNodes = transactions.map(transaction => (
  <Transaction
               key={ transaction.name + transaction.date + transaction.amount }
               {...transaction} />
  ));

  return (
    <div className="TransactionList">
      <SearchBarContainer />
      <div className="container">
        { transactionNodes }
      </div>
      <Link
            to="adding"
            className="btn btn-success"> Add
      </Link>
    </div>
    );
};

TransactionList.propTypes = {
  transactions: React.PropTypes.arrayOf(
    React.PropTypes.shape({
      name: React.PropTypes.string.isRequired,
      date: React.PropTypes.string.isRequired,
      amount: React.PropTypes.string.isRequired,
    }).isRequired
  ).isRequired,
};

export default TransactionList;
