import React, { Component } from 'react';
import DevTools from './DevTools';

class App extends Component {
  render() {
    return (
      <div>
        <div className="App">
          <div className="App-header">
            <h2>Simple Transaction</h2>
          </div>
          <div className="App-content">
            { this.props.children }
          </div>
        </div>
        <DevTools/>
      </div>
      );
  }
}

App.propTypes = {
  children: React.PropTypes.element.isRequired
};

export default App;
