import React from 'react';

const SearchBar = ({changeFilterFrom, changeFilterTo}) => {
  const changeFrom = (evt) => {
    const inputValue = evt.target.value;
    changeFilterFrom(inputValue);
  };
  const changeTo = (evt) => {
    const inputValue = evt.target.value;
    changeFilterTo(inputValue);
  };
  return (
    <div>
      <span>From:</span>
      <input
             type="date"
             onChange={ changeFrom } />
      <span>To:</span>
      <input
             type="date"
             onChange={ changeTo } />
    </div>
    );
};

SearchBar.propTypes = {
  changeFilterFrom: React.PropTypes.func.isRequired,
  changeFilterTo: React.PropTypes.func.isRequired
};

export default SearchBar;