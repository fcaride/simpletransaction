import React from 'react';
import { reduxForm } from 'redux-form';
import { Link } from 'react-router';
import { isValidDate } from '../utils'

const isNumberOnly = (val) => {
  return /^\d*\.?\d*$/.test(val);
};
export const validate = (values) => {
  const errors = {};

  if (!values.name) {
    errors.name = 'Please provide a name';
  } else if (!/^[a-zA-Z]+$/.test(values.name)) {
    errors.name = 'Please provide a real name';
  }

  if (!values.date) {
    errors.date = 'Please provide a date';
  } else if (!isValidDate(values.date)) {
    errors.date = 'Please provide a real date';
  } else if (new Date() >= new Date(values.date)) {
    errors.date = 'Please provide a date in the future';
  }

  if (!values.amount) {
    errors.amount = 'Please provide an amount';
  } else if (!isNumberOnly(values.amount)) {
    errors.amount = 'Please provide a real amount';
  }

  return errors;
};

const TransactionForm = ({addTransaction, fields: {name, date, amount}, handleSubmit}) => {
  return (
    <form
          className="TransactionForm-form"
          onSubmit={ handleSubmit(addTransaction) }>
      <div>
        <input
               type="text"
               placeholder="First Name"
               className="TransactionForm-input"
               {...name} />
        { name.touched && name.error && <div className="text-danger">
                                          { name.error }
                                        </div> }
      </div>
      <div>
        <input
               type="date"
               className="TransactionForm-input"
               {...date} />
        { date.touched && date.error && <div className="text-danger">
                                          { date.error }
                                        </div> }
      </div>
      <div>
        <input
               type="text"
               placeholder="Amount"
               className="TransactionForm-input"
               {...amount} />
        { amount.touched && amount.error && <div className="text-danger">
                                              { amount.error }
                                            </div> }
      </div>
      <button
              type="submit"
              className="btn btn-success">
        Submit
      </button>
      <Link
            to="/"
            className="btn btn-danger"> Cancel
      </Link>
    </form>
    );
}

TransactionForm.propTypes = {
  fields: React.PropTypes.object.isRequired,
  addTransaction: React.PropTypes.func.isRequired,
  handleSubmit: React.PropTypes.func.isRequired,
};

export default reduxForm({
  form: 'transaction',
  fields: ['name', 'date', 'amount'],
  validate,
})(TransactionForm);
