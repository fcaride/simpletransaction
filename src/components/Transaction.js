import React from 'react';

const Transaction = ({name, date, amount}) => {
  return (
    <div className="row">
      <div
           className="col-xs-2"
           id="name">
        { name }
      </div>
      <div
           className="col-xs-2"
           id="date">
        { date }
      </div>
      <div
           className="col-xs-2"
           id="amount">
        { amount }
      </div>
    </div>
    );
};

Transaction.propTypes = {
  name: React.PropTypes.string.isRequired,
  date: React.PropTypes.string.isRequired,
  amount: React.PropTypes.string.isRequired,
};

export default Transaction;
