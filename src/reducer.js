import { combineReducers } from 'redux';
import { ADD_TRANSACTION, RECEIVE_TRANSACTIONS, CHANGE_FROM_FILTER, CHANGE_TO_FILTER, FILTER_CHANGED } from './constants';
import { reducer as formReducer } from 'redux-form';

export function mainReducer(state = {
    transactions: [],
    filterFrom: "",
    filterTo: "",
    filteredTransactions: []
  } , action) {
  switch (action.type) {
    case RECEIVE_TRANSACTIONS:
      return Object.assign({}, state, {
        transactions: action.transactions,
        filteredTransactions: action.transactions
      });
    case ADD_TRANSACTION:
      return Object.assign({}, state, {
        transactions: [
          ...state.transactions,
          action.transaction,
        ],
        filteredTransactions: [
          ...state.filteredTransactions,
          action.transaction,
        ],
        filterFrom: "",
        filterTo: ""
      });
    case CHANGE_FROM_FILTER:
      return Object.assign({}, state, {
        filterFrom: action.inputValue
      });
    case CHANGE_TO_FILTER:
      return Object.assign({}, state, {
        filterTo: action.inputValue
      });
    case FILTER_CHANGED:
      return Object.assign({}, state, {
        filteredTransactions: action.filteredTransactions
      });
    default:
      return state;
  }
}

const reducer = combineReducers({
  main: mainReducer,
  form: formReducer
});

export default reducer;