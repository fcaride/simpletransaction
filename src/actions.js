import { ADD_TRANSACTION, RECEIVE_TRANSACTIONS, CHANGE_FROM_FILTER, CHANGE_TO_FILTER, FILTER_CHANGED } from './constants';
import { loadTransactions, saveTransaction } from './localStorage'

export function addTransaction() {
  return (dispatch, getState) => {
    const form = getState().form;
    const transaction = {
      name: form.transaction.name.value,
      date: form.transaction.date.value,
      amount: form.transaction.amount.value,
    };
    Promise.resolve(saveTransaction(transaction))
      .then(dispatch({
        type: ADD_TRANSACTION,
        transaction
      }));
  }
}

export function requestTransactions() {
  return (dispatch) => {
    Promise.resolve(loadTransactions())
      .then((transactions) => {
        dispatch({
          type: RECEIVE_TRANSACTIONS,
          transactions
        })
      });
  }
}

export function changeFilterFrom(inputValue) {
  return {
    type: CHANGE_FROM_FILTER,
    inputValue,
  };
}

export function changeFilterTo(inputValue) {
  return {
    type: CHANGE_TO_FILTER,
    inputValue,
  };
}

export function filterChanged() {
  return (dispatch, getState) => {
    const filterFrom = getState().main.filterFrom;
    const filterTo = getState().main.filterTo;
    const transactions = getState().main.transactions;
    var filteredTransactions = transactions.filter((transaction) => {
      const transactionDate = new Date(transaction.date);
      if (filterFrom !== "" && filterTo !== "")
        return transactionDate >= new Date(filterFrom) && transactionDate <= new Date(filterTo);
      if (filterFrom !== "")
        return transactionDate >= new Date(filterFrom);
      if (filterTo !== "")
        return transactionDate <= new Date(filterTo);
      if (filterTo === "" && filterFrom === "")
        return true;
      return false;
    });
    dispatch({
      type: FILTER_CHANGED,
      filteredTransactions
    });
  }
}