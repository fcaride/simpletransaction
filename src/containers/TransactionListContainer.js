import { connect } from 'react-redux';
import TransactionList from '../components/TransactionList';
import React from 'react';
import { requestTransactions } from '../actions';

class TransactionListContainer extends React.Component {
  componentWillMount() {
    if (!this.props.transactions || !this.props.transactions.length) {
      setTimeout(() => {
        this.props.requestTransactions();
      }, 0);
    }
  }


  render() {
    return (
      <TransactionList {...this.props}/>
    )
  }
}

TransactionListContainer.propTypes = {
  transactions: React.PropTypes.array.isRequired,
  requestTransactions: React.PropTypes.func.isRequired,
};


const mapStateToProps = (state) => ({
  transactions: state.main.filteredTransactions,
});

const mapDispatchToProps = (dispatch) => ({
  requestTransactions: () => dispatch(requestTransactions())
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(TransactionListContainer);
