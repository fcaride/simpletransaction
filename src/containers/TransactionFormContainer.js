import { connect } from 'react-redux';
import TransactionForm from '../components/TransactionForm';
import React from 'react';
import { addTransaction } from '../actions';
import { browserHistory } from 'react-router';



class TransactionFormContainer extends React.Component {
  render() {
    return (
      <TransactionForm {...this.props}/>
    )
  }
}

const mapDispatchToProps = (dispatch) => ({
  addTransaction: () => {
    dispatch(addTransaction());
    browserHistory.push('/');
  }
});

export default connect(
  () => ({}),
  mapDispatchToProps
)(TransactionFormContainer);
