import { connect } from 'react-redux';
import React from 'react';
import SearchBar from '../components/SearchBar'
import { changeFilterFrom, changeFilterTo, filterChanged } from '../actions'

class SearchBarContainer extends React.Component {

  render() {
    return (
      <SearchBar {...this.props} />

    )
  }
}


const mapDispatchToProps = (dispatch) => ({
  changeFilterFrom: (inputValue) => {
    dispatch(changeFilterFrom(inputValue));
    dispatch(filterChanged());
  },
  changeFilterTo: (inputValue) => {
    dispatch(changeFilterTo(inputValue));
    dispatch(filterChanged());
  }
});

export default connect(
  () => ({}),
  mapDispatchToProps
)(SearchBarContainer);
