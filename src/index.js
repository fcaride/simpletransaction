import React from 'react';
import ReactDOM from 'react-dom';
import TransactionListContainer from './containers/TransactionListContainer';
import TransactionFormContainer from './containers/TransactionFormContainer';
import AppContainer from './containers/AppContainer';
import './styles.css';
import configureStore from './configureStore';
import { Provider } from 'react-redux';
import { Router, Route, IndexRoute, browserHistory } from 'react-router';
const store = configureStore();

ReactDOM.render(
  <Provider store={ store }>
    <Router history={ browserHistory }>
      <Route
             path="/"
             component={ AppContainer }>
        <IndexRoute component={ TransactionListContainer }></IndexRoute>
        <Route
               path="adding"
               component={ TransactionFormContainer } />
      </Route>
    </Router>
  </Provider>,


  document.getElementById('root')
);
