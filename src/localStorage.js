export const loadState = () => {
  try {
    const serializedState = localStorage.getItem('transactions');
    const transactions = JSON.parse(serializedState).transactions;
    if (serializedState == null) {
      return undefined;
    }
    return {
      main: {
        transactions,
      }
    };
  } catch ( err ) {
    return undefined;
  }
};

export const saveState = (state) => {
  try {
    const serializedState = JSON.stringify(state);
    localStorage.setItem('transactions', serializedState);
  } catch ( err ) {
    console.log(err);
  }
}

export const loadTransactions = () => {
  try {
    const serializedState = localStorage.getItem('transactions');
    const transactions = JSON.parse(serializedState);
    if (serializedState == null) {
      return [];
    }
    return transactions;
  } catch ( err ) {
    return [];
  }
};

export const saveTransaction = (transaction) => {
  try {
    const serializedTran = localStorage.getItem('transactions');
    var transactions = [];
    if (serializedTran !== null) {
      transactions = JSON.parse(serializedTran);
    }
    const newSerializedTran = JSON.stringify([...transactions, transaction]);
    localStorage.setItem('transactions', newSerializedTran);
  } catch ( err ) {
    console.log(err);
  }
}
