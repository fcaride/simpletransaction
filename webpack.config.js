module.exports = {
  entry: './src/index.js',
  output: {
    path: './dist',
    filename: 'app.bundle.js'
  },
  module: {
    loaders: [
      {
        exclude: /node_modules/,
        loader: 'babel-loader'
      },
      {
        test: /\.scss$/,
        loaders: ["style", "css", "sass"]
      }

    ]
  },
  sassLoader: {
    includePaths: ["./styesheets"]
  }
}